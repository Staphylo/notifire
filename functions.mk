
%.o: %.c
	@echo -e "\e[34mCompiling \e[0m=> \e[32m'$<'\e[0m"
	@$(CC) $(CFLAGS) -DVERSION=\"$(VERSION)\" -c -o $@ $<

clean:
	@echo -e "\e[01;34mCleaning temporary files\e[0m"
	@rm -f *.o *~

info:
	@echo -e "\e[34mCC      \e[0m: ${CC}"
	@echo -e "\e[34mCFLAGS  \e[0m: ${CFLAGS}"
	@echo -e "\e[34mLDFLAGS \e[0m: ${LDFLAGS}"
	@echo -e "\e[34mVERSION \e[0m: ${VERSION}"

sep:
	@echo -e ""

