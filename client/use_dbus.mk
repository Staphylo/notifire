# If this extension is enabled then the notifications can be send via dbus 
# and works exactly like `notify-send`

CFLAGS  += -DUSE_DBUS `pkg-config --cflags dbus-1`
LDFLAGS += `pkg-config --libs dbus-1`
