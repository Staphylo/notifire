
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>

#include "../include/config.h"
#include "notification.h"
#include "remote.h"
#include "dbus.h"

static int useremote = 0;

static void
display_help(const char *prg)
{
    printf(
    "Client usage: %s [OPTIONS] [-r [password@]ip[:port]] title [body]\n"
    "\n"
    "Client Options:\n"
    "\n"
    "    -a --appname app     : set the app that launched the notification\n"
    "    -c --category cat    : add a category\n"
    "    -i --icon icon       : set an icon for the notification\n"
    "    -t --expire time     : time before expiration in second\n"
    "    -u --urgency lvl     : level of urgency (low,normal,critical\n"
    /*"    -x --posx x          : x position of the notification\n"*/
    /*"    -y --posy y          : y position of the notification\n"*/
    "\n"
    "Remote Options:\n"
    "\n"
    "    -r --remote ip:port  : send a remote notification\n"
#ifdef USE_JSON
    "    -j --json            : send the remote notification in json\n"
#endif /* USE_JSON */
    "    -s --ssl             : encrypt the notification with ssl\n"
    "\n"
    "Misc Options:\n"
    "\n"
    "    -h --help            : display this help\n"
    "    -v --version         : display version information\n"
    "\n"
    , prg);
}

static void
display_version()
{
    puts(
        "\n"
        "       Name : " NAME " client\n"
        "    Version : " VERSION "\n"
        "      Built : " __DATE__ ", " __TIME__ "\n"
        " Extensions :"
#ifdef USE_JSON
    " Json"
#endif
#ifdef USE_DBUS
    " DBus"
#endif
#ifdef USE_GAI
    " GAI"
#endif
        "\n"
        "   Homepage : http://www.staphylo.org/notifire\n"
        "      Email : samuel+notifire@staphylo.org\n"
        "  Copyright : Copyleft\n"
        "    License : Beerware\n"
    );
}

static int
parse_remote(const char *str)
{
    size_t i;
    int pass = -1;
    int ip = -1;

    for(i = 0; str[i]; ++i)
    {
        if(str[i] == '@')
        {
            pass = i;
            if(ip < pass)
                ip = -1;
        }
        if(str[i] == ':')
            ip = i;
    }

    if(pass > 0)
        remote_password = strndup(str, pass);
    if(ip < 1)
    {
        ip = i;
        remote_port = strdup("42058");
    }
    else
        remote_port = strndup(str + ip + 1, i - ip - 1);
    remote_ip = strndup(str + pass + 1, ip - pass - 1);

    return 0;
}

static int
parse_args(int argc, char *argv[], notification_t notif)
{
    const struct option lopt[] = {
        { "appname",  required_argument, 0, 'a' },
        { "category", required_argument, 0, 'c' },
        { "expire",   required_argument, 0, 't' },
        { "help",     no_argument,       0, 'h' },
        { "icon",     required_argument, 0, 'i' },
#ifdef USE_JSON
        { "json",     no_argument,       0, 'j' },
#endif /* USE_JSON */
        { "remote",   required_argument, 0, 'r' },
        { "ssl",      no_argument,       0, 's' },
        { "urgency",  required_argument, 0, 'u' },
        { "version",  no_argument,       0, 'v' },
        { NULL,       0,                 0, 0   }
    };

    int errors = 0;
    int opt;
    int idx;

    while((opt = getopt_long(argc, argv, "a:c:hi:jvr:t:u:", lopt, &idx)) != -1)
    {
        switch(opt)
        {
            case 'a':
                notif->app = strdup(optarg);
                break;
            case 'c':
                notif->category = strdup(optarg);
                break;
            case 'h':
                display_help(argv[0]);
                exit(EXIT_SUCCESS);
                break;
            case 'i':
                notif->icon = strdup(optarg);
                break;
#ifdef USE_JSON
            case 'j':
                remote_json = 1;
                break;
#endif /* USE_JSON */
            case 'r':
                if(parse_remote(optarg) == -1)
                    errors++;
                else
                    useremote = 1;
                break;
            case 's':
                puts("ssl unsupported for now");
                break;
            case 't':
                notif->expire = atoi(optarg);
                break;
            case 'u':
                if(!strcmp(optarg, "low")) notif->urgency = 0;
                else if(!strcmp(optarg, "normal")) notif->urgency = 1;
                else if(!strcmp(optarg, "critical")) notif->urgency = 2;
                else errors++;
                break;
            case 'v':
                display_version();
                exit(EXIT_SUCCESS);
                break;
            default:
                errors++;
                /*puts(argv[optind]);*/
        }
    }

    while(optind < argc)
    {
        if(!notif->title)
            notif->title = strdup(argv[optind]);
        else if(!notif->message)
            notif->message = strdup(argv[optind]);
        else
        {
            errors++;
            break;
        }
        optind++;
    }

    if(!notif->title)
        errors++;

    return errors;
}

int
main(int argc, char *argv[])
{
    int            error = 0;
    notification_t notif = notification_init();

    if(parse_args(argc, argv, notif) != 0)
    {
        display_help(argv[0]);
        return EXIT_FAILURE;
    }

    if(useremote)
    {
        if(remote_notify(notif) < 0)
        {
            puts("failed to send a remote notification");
            error = 1;
        }

        free(remote_ip);
        free(remote_password);
        free(remote_port);
    }
    else
    {
#ifdef USE_DBUS
        if(dbus_notify(notif) < 0)
        {
            puts("failed to send notification via dbus");
            error = 1;
        }
#else
        puts("no dbus support");
        error = 1;
#endif
    }

    notification_free(notif);

    return error;
}

