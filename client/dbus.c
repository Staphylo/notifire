#ifdef USE_DBUS

#include <stdio.h>
#include <dbus/dbus.h>

#include "notification.h"
#include "dbus.h"


static DBusConnection *
dbus_get_connection()
{
    DBusConnection *c;
    DBusError       e;
    int             ret;

    dbus_error_init(&e);

    c = dbus_bus_get(DBUS_BUS_SESSION, &e);
    if(dbus_error_is_set(&e))
    {
        printf("dbus connection error (%s)\n", e.message);
        dbus_error_free(&e);
        return NULL;
    }
    if(!c)
        return NULL;

    ret = dbus_bus_name_has_owner(c, DBUS_NAME, &e);
    if(dbus_error_is_set(&e) || ret != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER)
    {
        printf("no notification daemon own " DBUS_NAME "\n");
        dbus_error_free(&e);
        return NULL;
    }

    return c;
}

static int
dbus_send_notification(DBusConnection *c, notification_t notif)
{
    DBusMessage *msg;

    (void)msg;
    (void)c;
    (void)notif;

    return -1;
}

int
dbus_notify(notification_t notif)
{
    DBusConnection *c;

    if(!(c= dbus_get_connection()))
        return -1;

    return dbus_send_notification(c, notif);
}

#endif /* USE_DBUS */
