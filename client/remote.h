#ifndef REMOTE_H
#define REMOTE_H

#include "../include/remote_hdr.h"
#include "notification.h"

extern char *remote_ip;
extern char *remote_password;
extern char *remote_port;

# ifdef USE_JSON
extern int remote_json;
# endif /* USE_JSON */

int remote_notify(notification_t notif);

#endif /* !REMOTE_H */
