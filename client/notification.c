#include <stdlib.h>

/*#include "utils.h"*/
#include "notification.h"

notification_t
notification_init()
{
    notification_t notif = calloc(1, sizeof(struct notification_s));
    notif->urgency = 1;
    return notif;
}

void
notification_free(notification_t notif)
{
    free(notif->title);
    free(notif->message);
    free(notif->icon);
    free(notif->category);
    free(notif);
}
