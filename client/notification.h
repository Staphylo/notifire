#ifndef NOTIFICATION_H
#define NOTIFICATION_H

#include <stdint.h>

typedef struct notification_s
{
    char *title;
    char *message;
    char *icon;
    char *category;
    char *app;
    uint32_t expire;
    uint32_t urgency;
} *notification_t;

notification_t notification_init();
void           notification_free(notification_t notif);


#endif /* !NOTIFICATION_H */
