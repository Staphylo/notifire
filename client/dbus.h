#ifndef DBUS_H_
#define DBUS_H_

# ifdef USE_DBUS

#include "notification.h"

# define DBUS_TARGET "org.freedesktop.dunno"
# define DBUS_NAME   "org.freedesktop.Notifications"
# define DBUS_PATH   "/org/freedesktop/Notifications"
# define DBUS_METHOD "Notify"

int dbus_notify(notification_t notif);

# endif /* USE_DBUS */

#endif /* !DBUS_H_ */
