#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#ifdef USE_JSON
# include <json/json.h>
#endif /* USE_JSON */

#include "notification.h"
#include "remote.h"

char *remote_ip        = NULL;
char *remote_password  = NULL;
char *remote_port      = NULL;

#ifdef USE_JSON
int   remote_json      = 0;
#endif /* USE_JSON */

static int
remote_create_socket()
{
    int sock;
    struct addrinfo hints;
    struct addrinfo *res, *rp;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    sock = getaddrinfo(remote_ip, remote_port, &hints, &res);
    if(sock != 0)
    {
        printf("can't resolve notification server %s\n", gai_strerror(sock));
        return -1;
    }

    for(rp = res; rp != NULL; rp = rp->ai_next)
    {
        sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if(sock == -1)
            continue;
        if(connect(sock, rp->ai_addr, rp->ai_addrlen) != -1)
            break;
        close(sock);
    }

    freeaddrinfo(res);

    if(rp == NULL)
    {
        puts("can't connect to notification server");
        return -1;
    }

    return sock;
}

/*
keeping that if a version without gai must be made
static int
remote_create_socket()
{
    int sock;
    struct sockaddr_in addr;

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        return -1;

    addr.sin_family = AF_INET;
    addr.sin_port = htons(remote_port);

    if(inet_ntoa(remote_ip, &addr.sin_addr) == 0)
        if(remote_resolve(remote_ip, &addr.sin_addr) < 0)
            return -1;

    if(connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        return -1;

    return sock;
}*/

#define REMOTE_APPEND_HDR(Type, Size)                  \
    do {                                               \
        remote_id++;                                   \
        hdr.id = remote_id;                            \
        hdr.opcode = REMOTE_OP_##Type;                 \
        hdr.size = htons((uint16_t)Size);              \
        memcpy(buffer + offset, &hdr, REMOTE_HDR_LEN); \
        offset += REMOTE_HDR_LEN;                      \
    } while (0)

#define REMOTE_APPEND_STR(Type, Str)                   \
    do {                                               \
        if(Str)                                        \
        {                                              \
            tmp = strlen(Str);                         \
            REMOTE_APPEND_HDR(Type, tmp);              \
            memcpy(buffer + offset, Str, tmp);         \
            offset += tmp;                             \
        }                                              \
    } while (0)

#define REMOTE_APPEND_INT(Type, Int)                   \
    do {                                               \
        tmp = htonl(Int);                              \
        REMOTE_APPEND_HDR(Type, sizeof(tmp));          \
        memcpy(buffer + offset, &tmp, sizeof(tmp));    \
        offset += sizeof(tmp);                         \
    } while (0)

static ssize_t
remote_forge_notification(notification_t notif, char *buffer, size_t max)
{
    uint8_t remote_id = 0;
    size_t offset = 0;
    uint32_t tmp  = 0;
    struct remote_hdr_s hdr;

    (void)max;

    REMOTE_APPEND_STR(PASSWORD, remote_password);
    REMOTE_APPEND_STR(TITLE,    notif->title);
    REMOTE_APPEND_STR(MESSAGE,  notif->message);
    REMOTE_APPEND_STR(CATEGORY, notif->category);
    REMOTE_APPEND_STR(ICON,     notif->icon);
    REMOTE_APPEND_STR(APPNAME,  notif->app);
    REMOTE_APPEND_INT(URGENCY,  notif->urgency);
    REMOTE_APPEND_INT(EXPIRE,   notif->expire);
    REMOTE_APPEND_HDR(END,      0);

    return offset;
}

#undef REMOTE_APPEND_HDR
#undef REMOTE_APPEND_STR
#undef REMOTE_APPEND_INT

#ifdef USE_JSON

# define JSON_APPEND_STR(Key, Value)                                      \
    if(Value)                                                             \
        json_object_object_add(json, Key, json_object_new_string(Value))

# define JSON_APPEND_INT(Key, Value)                                       \
    json_object_object_add(json, Key, json_object_new_int(Value))

static ssize_t
remote_forge_json(notification_t notif, char *buffer, size_t max)
{
    struct json_object *json;
    const char *output;
    size_t len;

    (void)max;

    json = json_object_new_object();
    JSON_APPEND_STR("password", remote_password);
    JSON_APPEND_STR("title",    notif->title);
    JSON_APPEND_STR("message",  notif->message);
    JSON_APPEND_STR("icon",     notif->icon);
    JSON_APPEND_STR("appname",  notif->app);
    JSON_APPEND_STR("category", notif->category);
    JSON_APPEND_INT("expire",   notif->expire);
    JSON_APPEND_INT("urgency",  notif->urgency);
    output = json_object_to_json_string(json);
    puts(output);

    len = strlen(output);
    if(len > max)
    {
        json_object_put(json);
        return -1;
    }

    strncpy(buffer, output, max);
    json_object_put(json);

    return len;
}

# undef JSON_APPEND_STR
# undef JSON_APPEND_INT

#endif /* USE_JSON */

static int
remote_forge_packet(notification_t notif, char *buffer, size_t max)
{
#ifdef USE_JSON
    if(remote_json)
        return remote_forge_json(notif, buffer, max);
#endif /* USE_JSON */
    return remote_forge_notification(notif, buffer, max);
}

int
remote_notify(notification_t notif)
{
    int sock;
    size_t max = REMOTE_MAX_BUF_LEN;
    char buffer[max+1];
    ssize_t len;

    if((sock = remote_create_socket()) < 0)
        return -1;

    if((len = remote_forge_packet(notif, buffer, max)) < 0)
        return -1;

    if((len = send(sock, &buffer, len, 0)) < 0)
        return -1;

    close(sock);

    return 0;
}
