#ifndef XCB_H
#define XCB_H

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>

#define xm(Atom) extern xcb_atom_t Atom;
#include "atoms.def"
#undef xm

void atoms_init();
void window_move(xcb_window_t w, uint32_t x, uint32_t y);
void window_resize(xcb_window_t w, uint32_t width, uint32_t height);
void window_set_title(xcb_window_t w, const char *title);
void window_set_opacity(xcb_window_t w, uint32_t opacity);
void window_set_above(xcb_window_t w);
void window_set_notification(xcb_window_t w);

extern xcb_connection_t *c;

#endif /* !XCB_H */
