# This extension is for the remote notifications
# If enabled then the remote notification can be send in json
# See doc at http://www.staphylo.org/notifire

# Require json-c >= 1.0 if not segfaults in the lib happens when invalid json
# Too lazy to make a json checker that work for everything by myself

CFLAGS  += -DUSE_JSON
LDFLAGS += `pkg-config --libs json`

