#ifndef DBUS_H_
#define DBUS_H_

#define DBUS_NAME "org.freedesktop.Notifications"
#define DBUS_PATH "/org/freedesktop/Notifications"
#define TIME_BETWEEN_RECONNECTION 1

int  dbus_init();
void dbus_exit();
void dbus_receive();
void dbus_reconnect();

#endif /* !DBUS_H_ */
