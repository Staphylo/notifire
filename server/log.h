#ifndef LOG_H
#define LOG_H

#include <time.h>
#include <stdio.h>
#include <pthread.h>

#define LOG_DATE_FMT     "%T"
#define LOG_DATE_BUF_LEN 6 + 2 + 1 // 6 nums, 2 seps, 1 \0
#define LOG_DEFAULT_FILE stdout

void log_log(const char *__restrict fmt, ...);
void log_open();
void log_close();

#define log(Fmt, ...)                             \
    do {                                          \
        log_log(Fmt "\n", ##__VA_ARGS__);         \
    } while(0)

#define log_return(Fmt, ...)                      \
    do {                                          \
        log(Fmt, ##__VA_ARGS__);                  \
        return;                                   \
    } while(0)

#define log_return_val(Ret, Fmt, ...)             \
    do {                                          \
        log(Fmt, ##__VA_ARGS__);                  \
        return Ret;                               \
    } while(0)

#endif /* !LOG_H */
