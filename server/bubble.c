#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>

#include "log.h"
#include "xcb.h"
#include "config.h"
#include "timer.h"
#include "font.h"
#include "transition.h"
#include "bubble.h"

#define DEC_ANIMS()                      \
    do                                   \
    {                                    \
        pthread_mutex_lock(&animlock);   \
        --anims;                         \
        pthread_mutex_unlock(&animlock); \
    } while (0)

#define INC_ANIMS()                      \
    do                                   \
    {                                    \
        pthread_mutex_lock(&animlock);   \
        ++anims;                         \
        pthread_mutex_unlock(&animlock); \
        pthread_cond_signal(&animcond);  \
    } while (0);

xcb_connection_t            *c;

static xcb_screen_t         *s;
static font_t                font;
static bubble_t              list = NULL;

static int                   anims = 0;
static int                   timerwaiting = 0;
static pthread_mutex_t       animlock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t        animcond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t       addlock  = PTHREAD_MUTEX_INITIALIZER;

bubble_t
bubble_create()
{
    bubble_t b = calloc(1, sizeof(struct bubble_s));

    b->urgency = URGENCY_NORMAL;
    b->transon = config.transon;
    b->transoff = config.transoff;
    b->mouseon = NULL;
    b->mouseoff = NULL;

    return b;
}

void
bubble_free(bubble_t b)
{
    xcb_destroy_window(c, b->w);
    xcb_flush(c);
    free(b->appname);
    free(b->icon);
    free(b->title);
    free(b->message);
    free(b->category);
    free(b);
}

#ifdef DEBUG
static void
bubble_dump(bubble_t b)
{
    fprintf(stderr, "id       : %u\n", b->id);
    fprintf(stderr, "app      : %s\n", b->appname);
    fprintf(stderr, "icon     : %s\n", b->icon);
    fprintf(stderr, "title    : %s\n", b->title);
    fprintf(stderr, "message  : %s\n", b->message);
    fprintf(stderr, "category : %s\n", b->category);
    fprintf(stderr, "expire   : %d\n", b->expire);
    fprintf(stderr, "urgency  : %d\n", b->urgency);
    fprintf(stderr, "status   : %d\n", b->status);
}
#endif /* DEBUG */

static void
bubble_list_free()
{
    bubble_t b;
    while(list)
    {
        b = list;
        list = b->next;
        bubble_free(b);
    }
}

static size_t
sstrncpy(char *dest, const char *src, size_t n)
{
    size_t i;
    for(i = 0; i < n && src[i] != '\0'; i++)
        dest[i] = src[i];
    dest[i+1] = '\0';
    return i;
}

static size_t
text_find_split(const char *buf, size_t max)
{
    size_t i, last = 0;
    for(i = 0; i < max && buf[i]; ++i)
    {
        if(buf[i] == ' ')
            last = i;
        if(buf[i] == '\n')
        {
            last = i+1;
            break;
        }
    }
    if(!buf[i] || last == 0)
        last = i;
    return (last == 0) ? i : last;
}

static xcb_gcontext_t
bubble_create_gc(bubble_t b)
{
    xcb_gcontext_t gc;
    uint32_t mask;
    uint32_t vals[3];

    gc = xcb_generate_id(c);
    mask = XCB_GC_FOREGROUND |
           XCB_GC_BACKGROUND |
           XCB_GC_FONT;
    vals[0] = config.style[b->urgency].foreground;
    vals[1] = config.style[b->urgency].background;
    vals[2] = font.id;

    xcb_create_gc(c, gc, b->w, mask, vals);

    return gc;
}

// skip trailing space and LF, do it for title also
static void
bubble_draw(bubble_t b)
{
    // font 10x6 in this case
    int y             = 10 + config.padding;
    int x             = config.padding;
    size_t len;
    size_t maxchar    = config.cols - 1 -((2*config.padding)/font.avg_width);
    size_t current    = 0;
    size_t tlen       = sstrlen(b->title);
    size_t mlen       = sstrlen(b->message);
    const char *str;
    char buffer[maxchar+1];

    xcb_gcontext_t gc = bubble_create_gc(b);


    /*XftDraw *dr = XftDrawCreate(disp, (Drawable)b->w, visu, cm);*/
    /*XftColor c;*/

    /*XftColorAllocName(disp, DefaultVisual(disp, screen), cm, "white", &c);*/
    /*c.pixel = config.style[b->urgency].foreground;*/
    /*XSetForeground(disp, gc, config.style[b->urgency].foreground);*/
    /*XClearWindow(disp, b->w);*/

    if(tlen)
    {
        /*XDrawString(disp, b->w, gc, x, y, b->title, title);*/
        /*XftDrawStringUtf8(dr, &c, font, x, y, (unsigned char*)b->title, title);*/
        xcb_image_text_8(c, tlen, b->w, gc, x, y, b->title);
        y += 2 * font.height;
    }

    while(current < mlen)
    {
        str = b->message + current;
        /*while(*str == ' ')
        {
            str++;
            current++;
        }*/
        len = text_find_split(str, maxchar); //, &skip);
        sstrncpy(buffer, str, len);
        current += len;

        /*XDrawString(disl, b->w, gc, x, y, buffer, len);*/
        /*XftDrawStringUtf8(dr, &c, font, x, y, (unsigned char*)buffer, len);*/
        xcb_image_text_8(c, len, b->w, gc, x, y, buffer);
        y += font.height;
    }

    if(b->height < y)
    {
        b->height = y - font.height / 2;
        window_resize(b->w, b->width, b->height);
    }

    /*XftDrawDestroy(dr);*/
    xcb_free_gc(c, gc);
    xcb_flush(c);
}

// choose the side
void
bubble_list_add(bubble_t b)
{
    uint32_t mask;
    uint32_t vals[4];

    pthread_mutex_lock(&addlock);

    b->next = list;
    list = b;

    b->height = config.lines * font.height;
    if(font.max_width)
        b->width = config.cols * font.max_width;
    else
        b->width = config.default_width;

    // right side
    b->x = s->width_in_pixels - b->width - config.margin;
    b->y = -b->height;

    b->w = xcb_generate_id(c);
    mask = XCB_CW_BACK_PIXEL |
           XCB_CW_BORDER_PIXEL |
           XCB_CW_OVERRIDE_REDIRECT |
           XCB_CW_EVENT_MASK;
    vals[0] = config.style[b->urgency].background,
    vals[1] = config.style[b->urgency].border,
    vals[2] = 1;
    vals[3] = XCB_EVENT_MASK_BUTTON_PRESS |
              XCB_EVENT_MASK_ENTER_WINDOW |
              XCB_EVENT_MASK_LEAVE_WINDOW |
              XCB_EVENT_MASK_EXPOSURE;
    xcb_create_window(
        c,
        XCB_COPY_FROM_PARENT,
        b->w,
        s->root,
        b->x, b->y,
        b->width, b->height,
        config.style[b->urgency].border_size,
        XCB_WINDOW_CLASS_COPY_FROM_PARENT,
        s->root_visual,
        mask, vals
    );

    window_set_title(b->w, b->title);
    window_set_opacity(b->w, config.style[b->urgency].opacity);
    window_set_notification(b->w);
    if(config.above)
        window_set_above(b->w);
    xcb_map_window(c, b->w);

#ifdef DEBUG
    bubble_dump(b);
#endif /* DEBUG */

    xcb_flush(c);

    b->status = STATUS_TRANSON;
    b->timer = timer_current();
    INC_ANIMS();

    pthread_mutex_unlock(&addlock);
}


void
bubble_refresh()
{
    bubble_t             b = list;
    bubble_t             p = list;
    bubble_t             tmp = NULL;
    int                  move = 0;

    int                  offset = 9;

    pthread_mutex_lock(&animlock);
    while(anims == 0 && timerwaiting == 0)
        pthread_cond_wait(&animcond, &animlock);
    pthread_mutex_unlock(&animlock);

    while(b)
    {
        if(b->expire > 0 && b->status == STATUS_DISPLAY)
        {
            if(timer_is_over(b->timer, (double)b->expire))
            {
                b->timer = timer_current();
                b->status = STATUS_TRANSOFF;
                timerwaiting--;
                INC_ANIMS();
            }
        }
        else if(b->status == STATUS_TRANSON)
        {
            if(b->transon(b, offset))
            {
                DEC_ANIMS();
                b->y = offset;
                if(b->expire)
                    timerwaiting++;
                window_move(b->w, b->x, b->y);
            }
            move = 1;
        }
        else if(b->status == STATUS_TRANSOFF)
        {
            if(b->transoff(b, 0))//offset);
            {
                DEC_ANIMS();
                window_move(b->w, b->x, b->y);
            }
            move = 1;
        }

        if(b->status == STATUS_END)
        {
            tmp = b;
            if(list == b)
            {
                list = b->next;
                p = list;
                b = list;
            }
            else
            {
                p->next = b->next;
                b = b->next;
            }
            move = 1;
        }
        else
        {
            if(move)
                window_move(b->w, b->x, b->y + offset);

            offset += b->height + config.margin;

            p = b;
            b = b->next;
        }

        if(tmp)
        {
            bubble_free(tmp);
            tmp = NULL;
        }
    }

    xcb_flush(c);
    if(anims == 0)
        usleep(100000);

}

static bubble_t
bubble_find(xcb_window_t w)
{
    bubble_t b = list;

    while(b)
    {
        if(b->w == w)
            return b;
        b = b->next;
    }

    return NULL;
}

void
bubble_handle_event()
{
    xcb_generic_event_t *e;
    bubble_t b;

    e = xcb_wait_for_event(c);
    switch(e->response_type & ~0x80)
    {
        case XCB_BUTTON_PRESS:
            if(!(b = bubble_find(((xcb_button_press_event_t*)e)->event)))
                break;
            if(b->status != STATUS_TRANSOFF)
            {
                b->timer = timer_current();
                b->status = STATUS_TRANSOFF;
                INC_ANIMS();
            }
            // dbus close notification
            break;
        case XCB_ENTER_NOTIFY:
        case XCB_LEAVE_NOTIFY:
            if(!(b = bubble_find(((xcb_enter_notify_event_t*)e)->event)))
                break;

            if(e->response_type == XCB_ENTER_NOTIFY)
                window_set_opacity(b->w, 0xffffffff);
            else
                window_set_opacity(b->w,
                    ((config.style[b->urgency].opacity / 100.) * 0xffffffff));
            xcb_flush(c);
            // opacity transision
            break;
        case XCB_EXPOSE:
            if(!(b = bubble_find(((xcb_expose_event_t*)e)->window)))
                break;
            bubble_draw(b);
            break;
    }
    free(e);
}

int
bubble_init(const char *env)
{
    c = xcb_connect(env, NULL);
    if(!c)
    {
        fprintf(stderr, "Cannot open display %s\n", env);
        return -1;
    }

    s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;

    atoms_init();

    if(font_load(config.font, &font) == -1)
        return -1;

    return 0;
}

void
bubble_exit()
{
    bubble_list_free();
    font_free(font);
    xcb_disconnect(c);
}

