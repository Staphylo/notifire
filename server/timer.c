#include <X11/Xos.h>
#include "timer.h"

double
timer_current()
{
   struct timeval tv;
   X_GETTIMEOFDAY(&tv);
   return (double)tv.tv_sec + ((double)tv.tv_usec / 1000000);
}

int
timer_is_over(double begin, double time)
{
    return timer_current() - begin > time;
}

double
timer_elapsed(double begin)
{
    return timer_current() - begin;
}
