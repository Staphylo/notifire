/*
 * TODO:
 *   - securize dbus input with dbus_message_iter_get_arg_type(&args);
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <getopt.h>

#include "../include/config.h"
#include "config.h"
#include "log.h"
#include "dbus.h"
#include "bubble.h"
#include "remote.h"

#define THREAD_BEGIN(Name)          \
    const char *name = Name;        \
    prctl(PR_SET_NAME, name);       \
    log("thread %s started", name); \
    pthread_mutex_lock(&mutend);    \
    ++mutendval;                    \
    pthread_mutex_unlock(&mutend)

#define THREAD_END()                \
    pthread_mutex_lock(&mutend);    \
    --mutendval;                    \
    log("thread %s exited", name);  \
    pthread_mutex_unlock(&mutend);  \
    pthread_cond_signal(&condend);  \
    pthread_exit(NULL)

static int             isdaemon = 0;
static int             useremote = 0;

static pthread_mutex_t mutend = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t  condend = PTHREAD_COND_INITIALIZER;
static int             mutendval = 0;

static void *
thread_dbus_receive()
{
    THREAD_BEGIN("dbus");

    while(42)
        dbus_receive();

    THREAD_END();
}

static void *
thread_bubble_event()
{
    THREAD_BEGIN("event");

    while(42)
        bubble_handle_event();

    THREAD_END();
}

static void *
thread_bubble_animation()
{
    THREAD_BEGIN("animation");

    while(42)
    {
        bubble_refresh();
        usleep(10000);
    }

    THREAD_END();
}

static void *
thread_remote_listen()
{
    THREAD_BEGIN("remote");

    while(42)
        remote_listen();

    THREAD_END();
}

static void
display_help(const char *prg)
{
    printf(
    "Server usage: %s [-d] [-r [password@]ip[:port]] [OPTIONS]\n"
    "\n"
    "Server Options:\n"
    "\n"
    "    -d --daemon          : daemonize the notifier\n"
    "    -r --remote ip:port  : enable remote notification\n"
    "    -s --ssl             : enable ssl encryt for remote notification\n"
    "    -q --quit            : close the daemon if previously launched\n"
    "\n"
    "Misc Options:\n"
    "\n"
    "    -h --help            : display this help\n"
    "    -v --version         : display version information\n"
    "\n"
    , prg);
}

static void
display_version()
{
    puts(
        "\n"
        "       Name : " NAME " server\n"
        "    Version : " VERSION "\n"
        "      Built : " __DATE__ ", " __TIME__ "\n"
        "   Homepage : http://www.staphylo.org/notifire\n"
        "      Email : samuel+notifire@staphylo.org\n"
        "  Copyright : Copyleft\n"
        "    License : Beerware\n"
    );
}

static int
parse_remote(char *str)
{
    size_t i;
    int j;
    int pass = -1;
    int ip = -1;

    for(i = 0; str[i]; ++i)
    {
        if(str[i] == '@')
        {
            pass = i;
            if(ip < pass)
                ip = -1;
        }
        if(str[i] == ':')
            ip = i;
    }

    if(pass > 0)
        config.password = strndup(str, pass);
    if(ip < 1)
        ip = i;
    else
        config.port = atoi(str + ip + 1);
    config.ip = strndup(str + pass + 1, ip - pass - 1);

    if(pass > 0 && config.hidepassword)
    {
        for(j = 0; str[j+pass]; ++j)
            str[j] = str[j+pass+1];
        while(str[j])
            str[j++] = '\0';
    }

    return 0;
}

static int
parse_args(int argc, char *argv[])
{
    const struct option lopt[] = {
        { "help",       no_argument,       0, 'h' },
        { "daemon",     no_argument,       0, 'd' },
        { "quit",       no_argument,       0, 'q' },
        { "remote",     required_argument, 0, 'r' },
        { "version",    no_argument,       0, 'v' },
        { NULL,         0,                 0, 0   }
    };

    int iopt = 0;
    int errors = 0;
    char opt;

    while((opt = getopt_long(argc, argv, "qdvhfr:H:", lopt, &iopt)) != -1)
    {
        switch(opt)
        {
            case 'h':
                display_help(argv[0]);
                exit(EXIT_SUCCESS);
            case 'v':
                display_version();
                exit(EXIT_SUCCESS);
            case 'd':
                isdaemon = 1;
                break;
            case 'r':
                if(parse_remote(optarg) == -1)
                    errors++;
                else
                    useremote = 1;
                break;
            case 'q':
                puts("Not yet");
                errors++;
                break;
            case '?':
            case ':':
            default:
                errors++;
        }
    }

    return errors;
}

static int
daemon_daemonize()
{
    pid_t pid;

    pid = fork();
    if(pid < 0)
        return -1;
    else if(pid == 0)
    {
        chdir("/");
        umask(0);

        freopen("/dev/null", "a+", stdout);
        freopen("/dev/null", "r",  stdin);
        freopen("/dev/null", "a+", stderr);
    }
    else
        exit(EXIT_SUCCESS);

    return 0;
}

static int
daemon_run()
{
    pthread_t thdbus;
    pthread_t thevent;
    pthread_t thanim;
    pthread_t thremote;

    if(dbus_init() != 0)
        goto dbus_error;
    if(bubble_init(NULL) != 0)
        goto bubble_error;
    if(useremote)
        if(remote_init() == -1)
            goto remote_error;
    if(isdaemon)
        if(daemon_daemonize() == -1)
            goto fork_error;

    pthread_create(&thdbus,  NULL, &thread_dbus_receive, NULL);
    pthread_create(&thevent, NULL, &thread_bubble_event, NULL);
    pthread_create(&thanim,  NULL, &thread_bubble_animation,  NULL);
    if(useremote)
        pthread_create(&thremote, NULL, &thread_remote_listen, NULL);

    // as i am not using join i need to be sure that the threads have set
    // mutendval
    usleep(10000);
    pthread_mutex_lock(&mutend);
    while(mutendval > 0)
        pthread_cond_wait(&condend, &mutend);
    pthread_mutex_unlock(&mutend);

    if(useremote)
        remote_exit();
    bubble_exit();
    dbus_exit();

    return EXIT_SUCCESS;

fork_error:
    log("Cannot fork");
remote_error:
    if(useremote)
        remote_exit();
bubble_error:
    bubble_exit();
dbus_error:
    dbus_exit();
    return EXIT_FAILURE;
}

int
main(int argc, char *argv[])
{
    int res;

    config_init();
    log_open();

    if(parse_args(argc, argv) != 0)
    {
        display_help(argv[0]);
        return EXIT_FAILURE;
    }

    res = daemon_run();

    log_close();

    return res;
}

