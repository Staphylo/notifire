#include "timer.h"
#include "config.h"
#include "bubble.h"
#include "transition.h"

static int
lerp(int begin, int end, double pc)
{
    return (end - begin) * pc + begin;
}

int transition_none(bubble_t b, int offset)
{
    (void)offset;
    b->status++;
    b->timer = timer_current();
    return 1;
}

int
transition_slide_on(bubble_t b, int offset)
{
    if(timer_is_over(b->timer, config.transition))
    {
        b->status = STATUS_DISPLAY;
        b->timer = timer_current();
        return 1;
    }
    else
    {
        b->y = lerp(
            0 - b->height - config.margin,
            config.margin + offset,
            timer_elapsed(b->timer) / config.transition
        );
        return 0;
    }
}

int
transition_slide_off(bubble_t b, int offset)
{
    if(timer_is_over(b->timer, config.transition))
    {
        b->status = STATUS_END;
        b->timer = timer_current();
        return 1;
    }
    else
    {
        b->y = lerp(
            config.margin + offset,
            0 - b->height - config.margin,
            timer_elapsed(b->timer) / config.transition
        );
        return 0;
    }
}
