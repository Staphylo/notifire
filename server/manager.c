
#include "log.h"
#include "xcb.h"
#include "font.h"
#include "config.h"
#include "bubble.h"
#include "manager.h"

static xcb_screen_t  *s;
static font_t        font;

static bubble_list_t list;

int
manager_init(const char *env)
{
    c = xcb_connect(env, NULL);
    list = NULL;
    if(!c)
    {
        fprintf(stderr, "Cannot open display %s\n", env);
        return -1;
    }

    s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;

    atoms_init();

    if(font_load(config.font, &font) == -1)
        return -1;

    return 0;
}

void
manager_exit()
{
    /*bubble_list_free();*/
    font_free(font);
    xcb_disconnect(c);
}
