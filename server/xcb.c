#include <stdlib.h>
#include <string.h>
#include "xcb.h"

#define xm(Atom) xcb_atom_t Atom;
#include "atoms.def"
#undef xm

void
window_move(xcb_window_t w, uint32_t x, uint32_t y)
{
    uint32_t vals[2] = { x, y };
    xcb_configure_window (
        c, w, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, vals
    );
}

void
window_resize(xcb_window_t w, uint32_t width, uint32_t height)
{
    uint32_t vals[2] = { width, height };
    xcb_configure_window (
        c, w, XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, vals
    );
}

void
window_set_title(xcb_window_t w, const char *title)
{    
    xcb_change_property (c, XCB_PROP_MODE_REPLACE, w,
            XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8,
            strlen(title), title);
}

void
window_set_opacity(xcb_window_t w, uint32_t opacity)
{
    xcb_change_property (c, XCB_PROP_MODE_REPLACE, w,
            _NET_WM_WINDOW_OPACITY, XCB_ATOM_CARDINAL, 32,
            1, &opacity);
}

void
window_set_above(xcb_window_t w)
{
    xcb_change_property (c, XCB_PROP_MODE_REPLACE, w,
            _NET_WM_STATE, XCB_ATOM_ATOM, 32,
            1, &_NET_WM_STATE_ABOVE);
}

void
window_set_notification(xcb_window_t w)
{
    xcb_change_property (c, XCB_PROP_MODE_REPLACE, w,
            _NET_WM_WINDOW_TYPE, XCB_ATOM_ATOM, 32,
            1, &_NET_WM_WINDOW_TYPE_NOTIFICATION);
}

/* wrong way but too lazy to do otherwise */
void
atoms_init()
{
    xcb_intern_atom_cookie_t cs;
    xcb_intern_atom_reply_t *r;

#define xm(Atom)                                                      \
    cs = xcb_intern_atom_unchecked(c, 0, strlen(#Atom), #Atom); \
    if((r = xcb_intern_atom_reply(c, cs, NULL)))                      \
    {                                                                 \
        Atom = r->atom;                                               \
        free(r);                                                      \
    } 
#include "atoms.def"
#undef xm
}

