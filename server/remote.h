#ifndef REMOTE_H
#define REMOTE_H

#include "../include/remote_hdr.h"

#define REMOTE_QUEUE_LEN    5
#define REMOTE_RETRY_NUMBER 5
#define REMOTE_RETRY_SLEEP  5

void remote_listen();
int  remote_init();
void remote_exit();

#endif /* !REMOTE_H */
