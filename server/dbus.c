#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dbus/dbus.h>

#include "log.h"
#include "../include/config.h"
#include "config.h"
#include "bubble.h"
#include "dbus.h"

static DBusConnection* connection = NULL;
static dbus_uint32_t serial = 0;
static dbus_uint32_t id = 0;

static int
dbus_get_connection()
{
    if(connection)
        log_return_val(1, "Connection already established");

    int ret;
    DBusError error;
    dbus_error_init(&error);

    connection = dbus_bus_get(DBUS_BUS_SESSION, &error);
    if(dbus_error_is_set(&error))
    {
        log("DBus Connection Error (%s)", error.message);
        dbus_error_free(&error);
        return -1;
    }
    if(!connection)
        return -1;

    if(dbus_bus_name_has_owner(connection, DBUS_NAME, &error))
    {
        log(DBUS_NAME " is already owned");
        log("An other notification program may be launched");
        dbus_error_free(&error);
        return -1;
    }

    ret = dbus_bus_request_name(connection, DBUS_NAME,
            DBUS_NAME_FLAG_REPLACE_EXISTING, &error);
    if(dbus_error_is_set(&error))
    {
        log("DBus Name Error (%s)", error.message);
        dbus_error_free(&error);
    }
    if(ret != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER)
        log_return_val(-1, "Don't own " DBUS_NAME);

    log_return_val(0, "DBus Connection Established");
}

static void
dbus_notify(DBusMessage *msg)
{
    bubble_t b = bubble_create();

    DBusMessage *reply;
    DBusMessageIter args;
    DBusMessageIter array;
    DBusMessageIter entry;
    DBusMessageIter lol;

    const char *str;
    dbus_int32_t num;

    serial++;
    id++;

    b->x = config.padding;
    b->y = config.padding;

    
    // Application name
    dbus_message_iter_init(msg, &args);
    dbus_message_iter_get_basic(&args, &str);
    b->appname = strdup(str);

    // Id 
    dbus_message_iter_next(&args);
    dbus_message_iter_get_basic(&args, &num);
    b->id = (num == 0) ? id : (unsigned int)num;

    // Icon
    dbus_message_iter_next(&args);
    dbus_message_iter_get_basic(&args, &str);
    if(strlen(str) > 0)
        b->icon = strdup(str);

    // Title
    dbus_message_iter_next(&args);
    dbus_message_iter_get_basic(&args, &str);
    b->title = strdup(str);

    // Message
    dbus_message_iter_next(&args);
    dbus_message_iter_get_basic(&args, &str);
    b->message = strdup(str);

    // Actions
    dbus_message_iter_next(&args);
    dbus_message_iter_recurse(&args, &array);
    while(dbus_message_iter_get_arg_type(&array))
    {
        dbus_message_iter_next(&array);
        log("dbus notiication action array unused");
    }

    // Hints
    dbus_message_iter_next(&args);
    dbus_message_iter_recurse(&args, &array);
    while(dbus_message_iter_get_arg_type(&array))
    {
        dbus_message_iter_recurse(&array, &entry);
        {
            dbus_message_iter_get_basic(&entry, &str);
            dbus_message_iter_next(&entry);
            dbus_message_iter_recurse(&entry, &lol);
            if(strncmp("urgency", str, 7) == 0 &&
                dbus_message_iter_get_arg_type(&lol) == DBUS_TYPE_BYTE)
            {
                dbus_message_iter_get_basic(&lol, &num);
                b->urgency = num % 3;
            }        
            else if(strncmp("x", str, 1) == 0)
            {
            }
            else if(strncmp("y", str, 1) == 0)
            {
            }
            else if(strncmp("category", str, 8) == 0 &&
                dbus_message_iter_get_arg_type(&lol) == DBUS_TYPE_STRING)
            {
                /*dbus_message_iter_get_basic(&lol, &str);*/
                /*printf("Category : %s\n", str);*/
            }
            else
            {
                /*printf("%s - %d\n", str, b->urgency);*/
                log("unused property : %s (%c)", str,
                        dbus_message_iter_get_arg_type(&lol));
            }
        }
        dbus_message_iter_next(&array);
    }

    // Expire
    dbus_message_iter_next(&args);
    dbus_message_iter_get_basic(&args, &num);
    b->expire = (num < -1) ? config.expire : num;

    // add the bubble to the list
    bubble_list_add(b);

    // Response
    reply = dbus_message_new_method_return(msg);
    dbus_message_iter_init_append(reply, &args);
    if(!dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT32, &id) ||
       !dbus_connection_send(connection, reply, &serial))
        log_return("Notify response failed (%d)", id);
    dbus_message_unref(reply);
}

static void
dbus_getcapabilities(DBusMessage *msg)
{
    DBusMessage* reply;
    DBusMessageIter args;
    DBusMessageIter subargs;

    static const char *caps[] = { "body" };
    id++;

    reply = dbus_message_new_method_return(msg);
    if(!reply)
        return;

    dbus_message_iter_init_append(reply, &args);

    if (!dbus_message_iter_open_container(&args, DBUS_TYPE_ARRAY,
                DBUS_TYPE_STRING_AS_STRING, &subargs ) ||
        !dbus_message_iter_append_basic(&subargs, DBUS_TYPE_STRING, caps) ||
        !dbus_message_iter_close_container(&args, &subargs) ||
        !dbus_connection_send(connection, reply, &id))
        return;

    dbus_message_unref(reply);
}

static void
dbus_getserverinformation(DBusMessage *msg)
{
    DBusMessage* reply;
    DBusMessageIter args;

    static const char* info[] = {
        NAME,
        NAME,
        VERSION,
        VERSION
    };

    serial++;

    reply = dbus_message_new_method_return(msg);

    dbus_message_iter_init_append(reply, &args);
    if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &info[0]) ||
        !dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &info[1]) ||
        !dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &info[2]) ||
        !dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &info[3]) ||
        !dbus_connection_send(connection, reply, &serial))
        return;

    dbus_message_unref(reply);
}

static void
dbus_closenotification(DBusMessage *msg)
{
    (void)msg;
}

int
dbus_init()
{
    return dbus_get_connection();
    /*if(dbus_get_signals("Notify") != 0)
        return -1;;*/
}

void
dbus_exit()
{
    /*if(connection)*/
    /*    dbus_connection_close(connection);*/
}

void
dbus_reconnect()
{
    log("Connection to dbus lost. Reconnecting");
    while(dbus_get_connection() == -1)
    {
        log("Attempt failed.");
        sleep(TIME_BETWEEN_RECONNECTION);
    }
}

void
dbus_receive()
{
    DBusMessage *msg;
    
    if(!dbus_connection_get_is_connected(connection))
        dbus_reconnect();

    dbus_connection_read_write(connection, -1);

    while(42)
    {
        msg = dbus_connection_pop_message(connection);

        if(!msg)
            break;

        if(dbus_message_is_method_call(msg, DBUS_NAME, "Notify"))
            dbus_notify(msg);
        if(dbus_message_is_method_call(msg, DBUS_NAME, "GetCapabilities"))
            dbus_getcapabilities(msg);
        if(dbus_message_is_method_call(msg, DBUS_NAME, "GetServerInformation"))
            dbus_getserverinformation(msg);
        if(dbus_message_is_method_call(msg, DBUS_NAME, "CloseNotification"))
            dbus_closenotification(msg);

        dbus_message_unref(msg);
    }

    dbus_connection_flush(connection);
}
