#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>
#include "bubble.h"
#include "../include/remote_hdr.h"

enum
{
    URGENCY_LOW = 0,
    URGENCY_NORMAL = 1,
    URGENCY_CRITICAL = 2
};

typedef struct bubble_style_s
{
    uint32_t border;
    uint32_t foreground;
    uint32_t background;
    uint32_t border_size;
    uint32_t opacity;
} bubble_style_s;

typedef struct config_s
{
    bubble_style_s style[3];
    int      padding;
    int      margin;
    transition_f transon;
    transition_f transoff;
    double   transition;
    double   expire;
    int      above;
    int      cols;
    int      lines;
    int      default_width;
    char    *ip;
    int      port;
    char    *password;
    char    *logfile;
    char     hidepassword;
    char     font[100];
} config_s;

extern config_s config;

void config_init();

#endif /* !CONFIG_H */
