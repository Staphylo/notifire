#ifndef TIMER_H
#define TIMER_H

/*
 * the time is express as the timestamp (dot) microseconds
 */

/*
 * return 1 if the time between the begining and now is elapsed, 0 otherwise
 */
int    timer_is_over(double begin, double time);

/* 
 * return current time
 */
double timer_current();

/*
 * return the elapsed time since begin
 */
double timer_elapsed(double begin);

#endif /* !TIMER_H */
