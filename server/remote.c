#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifdef USE_JSON
# include <json/json.h>
#endif

#include "log.h"
#include "config.h"
#include "remote.h"
#include "bubble.h"

static int sock;

static int
remote_check_password(const char *pass, const char *try, size_t len)
{
    size_t i;

    if(!pass)
        return 0;
    if(!len)
        return -1;

    for(i = 0; i < len; ++i)
    {
        if(!pass[i] || pass[i] != try[i])
            return -1;
    }

    if(pass[i])
        return -1;

    return 0;
}

/*static int
remote_check_string(const char *str, size_t len)
{
    const char *c = str;

    while(c - str < len)
    {
        if(*c < 0 || *c > 127)
            return -1;
        ++c;
    }

    return 0;
}*/

// instead of a switch case the enum vals could be an offset of the field and
// then use a mask to know if it's an integer or not
static int
remote_parse_data(const remote_hdr_t hdr, const char *buffer, bubble_t b)
{
    uint32_t tmp;
    switch(hdr->opcode)
    {
        case REMOTE_OP_PASSWORD:
            if(!remote_check_password(config.password, buffer, hdr->size))
            {
                b->status = STATUS_BEGIN;
                log("Authentification sucessful");
            }
            else
                log_return_val(-1, "Wrong password");
            break;
        case REMOTE_OP_CATEGORY:
            b->category = strndup(buffer, hdr->size);
            break;
        case REMOTE_OP_TITLE:
            b->title = strndup(buffer, hdr->size);
            break;
        case REMOTE_OP_MESSAGE:
            b->message = strndup(buffer, hdr->size);
            break;
        case REMOTE_OP_ICON:
            b->icon = strndup(buffer, hdr->size);
            break;
        case REMOTE_OP_APPNAME:
            b->appname = strndup(buffer, hdr->size);
            break;
        case REMOTE_OP_EXPIRE:
            memcpy(&tmp, buffer, hdr->size);
            b->expire = ntohl(tmp);
            break;
        case REMOTE_OP_URGENCY:
            memcpy(&tmp, buffer, hdr->size);
            b->urgency = ntohl(tmp);
            if(b->urgency > URGENCY_CRITICAL)
                b->urgency = URGENCY_NORMAL;
            break;
        case REMOTE_OP_END:
            return 1;
        default:
            log_return_val(-1, "Unknown opcode %0X, failure", hdr->opcode);
    }

    return 0;
}

#ifdef DEBUG
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
static void
remote_dump_hdr(remote_hdr_t hdr, const char *buffer, ssize_t len)
{
    char    tmp;
    ssize_t offset = 0;
    ssize_t start  = 0;
    ssize_t end    = 0;

    fprintf(stderr, "Id     : %u\n", hdr->id);
    fprintf(stderr, "Opcode : 0x%02x\n", hdr->opcode);
    fprintf(stderr, "Size   : %u\n", hdr->size);
    fprintf(stderr, "Data   : ");

    start = offset;
    end = offset + hdr->size;
    while(offset < end && offset < len)
    {
        fprintf(stderr, "%02x ", (char)buffer[offset]);
        ++offset;
    }

    offset = start;
    fprintf(stderr, "\n         ");
    while(offset < end && offset < len)
    {
        tmp = (char)buffer[offset];
        if(tmp < 20 || tmp > 126)
            tmp = '.';
        fprintf(stderr, "%c  ", tmp);
        ++offset;
    }

    fputc('\n', stderr);
}

static void
remote_dump_packet(const char *buffer, ssize_t len)
{
    ssize_t             offset = 0;
    struct remote_hdr_s hdr;

    while(offset < len)
    {
        if(len - offset < (ssize_t)REMOTE_HDR_LEN)
            return;
        memcpy(&hdr, buffer + offset, REMOTE_HDR_LEN);
        hdr.size = htons(hdr.size);
        offset += REMOTE_HDR_LEN;
        remote_dump_hdr(&hdr, buffer + offset, len-offset);
        offset += hdr.size;
    }
}
#pragma GCC diagnostic pop
#endif /* DEBUG */

static int
remote_parse_buffer(const char *buffer, ssize_t len, bubble_t b)
{
    uint8_t id = 0;
    ssize_t offset = 0;
    struct remote_hdr_s hdr;

#ifdef DEBUG
    /*remote_dump_packet(buffer, len);*/
#endif

    while(offset < len)
    {
        id++;
        if(offset + (ssize_t)REMOTE_HDR_LEN > len)
            log_return_val(-1, "invalid header");
        memcpy(&hdr, buffer + offset, REMOTE_HDR_LEN);
        hdr.size = ntohs(hdr.size);
        offset += REMOTE_HDR_LEN;
#ifdef DEBUG
        remote_dump_hdr(&hdr, buffer + offset, len - offset);
#endif
        if(id != hdr.id)
            log_return_val(-1, "invalid id (%u)", hdr.id);
        if(offset + hdr.size > len)
            log_return_val(-1, "invalid data size (%u)", hdr.size);
        if(remote_parse_data(&hdr, buffer + offset, b) < 0)
            return -1;
        offset += hdr.size;
    }

    return 0;
}

#ifdef USE_JSON
# define JSON_EXTRACT(Target, Type)                                      \
    if(!strcmp(key, Target) && json_object_is_type(val, json_type_##Type)) \
    {

# define JSON_END_EXTRACT()                                                 \
        continue;                                                          \
    }

# define JSON_EXTRACT_STR(Target, Var)                                   \
    JSON_EXTRACT(Target, string)                                         \
        Var = strdup(json_object_get_string(val));                         \
    JSON_END_EXTRACT()

static int
remote_parse_json(const char *buffer, bubble_t b)
{
    struct json_object *json;
    uint32_t tmpi;

    /*if(!json_is_valid(buffer))*/
    /*    log_return_val(-1, "invalid json");*/

# ifdef DEBUG
    fprintf(stderr, "%s\n", buffer);
# endif /* DEBUG */

    json = json_tokener_parse(buffer);
    if(!json)
        log_return_val(-1, "invalid json");

    json_object_object_foreach(json, key, val)
    {
        JSON_EXTRACT("password", string)
            if(!strcmp(config.password, json_object_get_string(val)))
            {
                b->status = STATUS_BEGIN;
                log("Authentification sucessful");
            }
            else
                log_return_val(-1, "Wrong password");
        JSON_END_EXTRACT()
        JSON_EXTRACT_STR("title",    b->title);
        JSON_EXTRACT_STR("message",  b->message);
        JSON_EXTRACT_STR("appname",  b->appname);
        JSON_EXTRACT_STR("icon",     b->icon)
        JSON_EXTRACT_STR("category", b->category)
        JSON_EXTRACT("expire", int)
            b->expire = (uint32_t)json_object_get_int(val);
        JSON_END_EXTRACT()
        JSON_EXTRACT("urgency", int)
            tmpi = (uint32_t)json_object_get_int(val);
            if(tmpi < 3)
                b->urgency = tmpi;
        JSON_END_EXTRACT()
        log("json parsing : unused key %s", key);
    }

    json_object_put(json);

    return 0;
}

# undef JSON_EXTRACT_STR
# undef JSON_EXTRACT
# undef JSON_END_EXTRACT

#endif /* USE_JSON */

static int
remote_parse_packet(const char *buffer, ssize_t len, bubble_t b)
{
#ifdef USE_JSON
    if(len < 1)
        return -1;
    if(buffer[0] == '{')
    {
        log("json remote notification");
        return remote_parse_json(buffer, b);
    }
    else
        log("default remote notification");
#endif /* USE_JSON */
    return remote_parse_buffer(buffer, len, b);
}

void
remote_listen()
{
    int client;
    socklen_t clientlen;
    struct sockaddr_in addr;
    char buffer[REMOTE_MAX_BUF_LEN + 1];
    ssize_t max = REMOTE_MAX_BUF_LEN;
    ssize_t len;
    bubble_t b;

    clientlen = sizeof(addr);
    if((client = accept(sock, (struct sockaddr *)&addr, &clientlen)) < 0)
        log_return("cannot accept client");

    log("client connected from %s:%u", inet_ntoa(addr.sin_addr),
            addr.sin_port);

    if((len = recv(client, buffer, max, 0)) < 0)
        log_return("error receiving from client");

    close(client);

    b = bubble_create();
    if(config.password)
        b->status = STATUS_ERROR;

    if(remote_parse_packet(buffer, len, b) < 0 ||
       b->status != STATUS_BEGIN ||
       b->title == NULL)
    {
        log_return("invalid notification");
        bubble_free(b);
    }
    else
        bubble_list_add(b);
}

int
remote_init()
{
    int try = 0;
    struct sockaddr_in addr;

    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        log_return_val(-1, "cannot create remote socket");

    memset(&addr, sizeof(struct sockaddr_in), 0);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(config.ip);
    addr.sin_port = config.port;

    while(bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        if(try >= REMOTE_RETRY_NUMBER)
            log_return_val(-1, "cannot bind remote socket");
        try++;
        log("%d failed to bind remote socket. retrying in %ds ...",
                try, REMOTE_RETRY_SLEEP);
        sleep(REMOTE_RETRY_SLEEP);
    }

    listen(sock, REMOTE_QUEUE_LEN);

    return 0;
}

void
remote_exit()
{
    if(sock >= 0)
        close(sock);
}

