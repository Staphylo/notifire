#ifndef MANAGER_H
#define MANAGER_H

typedef struct bubble_node_s *bubble_node_t;
struct bubble_node_s
{
    bubble_t      bubble;
    bubble_node_t prev;
    bubble_node_t next;
};

typedef struct bubble_list_s
{
    bubble_node_t first;
    bubble_node_t last;
    size_t        len;
} *bubble_list_t;

int  manager_init(const char *env);
void manager_exit();

#endif /* !MANAGER_H */
