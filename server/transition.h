#ifndef TRANSITION_H
#define TRANSITION_H

#include "bubble.h"

int transition_none(bubble_t b, int offset);

int transition_slide_on(bubble_t b, int offset);
int transition_slide_off(bubble_t b, int offset);


#endif /* !TRANSITION_H */
