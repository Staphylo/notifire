#ifndef BUBBLE_H
#define BUBBLE_H

#include <xcb/xcb.h>
#include <stdint.h>

typedef struct bubble_s *bubble_t;
typedef int (*transition_f)(bubble_t b, int offset);
typedef int (*mouse_f)(bubble_t b, int x, int y);

enum
{
    STATUS_ERROR    = -1,
    STATUS_BEGIN    = 0,
    STATUS_TRANSON  = 1,
    STATUS_DISPLAY  = 2,
    STATUS_TRANSOFF = 3,
    STATUS_END      = 4
};

struct bubble_s
{
    xcb_window_t w;
    unsigned int id;
    char *appname;
    char *icon;
    char *title;
    char *message;
    char *category;
    int x;
    int y;
    int width;
    int height;
    uint32_t urgency;
    uint32_t expire;
    int status;
    double timer;
    transition_f transon;
    transition_f transoff;
    mouse_f mouseon;
    mouse_f mouseoff;
    struct bubble_s *next;
};

int  bubble_init();
void bubble_exit();
void bubble_list_add(bubble_t b);
void bubble_refresh();
void bubble_handle_event();
void bubble_free();
bubble_t bubble_create();

// safe strlen
#define sstrlen(Str) ((Str) ? strlen(Str) : 0)

#endif /* !BUBBLE_H */
