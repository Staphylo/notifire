#include <stdio.h>
#include <stdarg.h>

#include "log.h"
#include "config.h"

static FILE           *logfile;
static pthread_mutex_t loglock;
static char            datebuf[LOG_DATE_BUF_LEN];

void
log_log(const char *__restrict fmt, ...)
{
    time_t t = time(NULL);
    va_list ap;
    pthread_mutex_lock(&loglock);
    strftime(datebuf, LOG_DATE_BUF_LEN, LOG_DATE_FMT, localtime(&t));
    fprintf(logfile, "[%s] ", datebuf);
    va_start(ap, fmt);
    vfprintf(logfile, fmt, ap);
    va_end(ap);
    pthread_mutex_unlock(&loglock);
}

void
log_open()
{
    if(config.logfile)
    {
        logfile = fopen(config.logfile, "a+");
        if(logfile)
            setvbuf(logfile, NULL, _IOLBF, 0);
        else
        {
            logfile = LOG_DEFAULT_FILE;
            log("Could not open %s as log file, using stdout instead",
                config.logfile);
        }
    }
    else
        logfile = LOG_DEFAULT_FILE;
}

void
log_close()
{
    if(logfile != stderr)
        fclose(logfile);
}

