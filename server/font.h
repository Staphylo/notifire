#ifndef FONT_H
#define FONT_H

#include <xcb/xcb.h>

typedef struct font_s
{
    xcb_font_t              id;
    xcb_query_font_reply_t *info;
    xcb_charinfo_t         *data;
    uint32_t                height;
    uint32_t                max_width;
    uint32_t                avg_width;
} font_t;

int  font_load(const char *name, font_t *font);
void font_free(font_t font);

#endif /* !FONT_H */
