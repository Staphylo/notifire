#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "xcb.h"
#include "font.h"
#include "config.h"

static void
font_fillinfo(font_t *font)
{
    /*int16_t tmp;
    size_t i;*/

    font->height = font->info->font_ascent + font->info->font_descent;
    font->max_width = font->info->max_bounds.character_width;
    font->avg_width = font->max_width;

    /*if(font->data)
    {
        for(i = 0; i < sizeof(font->data); ++i)
        {
            tmp = font->data[i].character_width;
            if(tmp > font->max_width)
                font->max_width = tmp;
            font->avg_width += tmp;
        }
        font->avg_width /= sizeof(font->data);
    }*/
}

int
font_load(const char *name, font_t *font)
{
    xcb_void_cookie_t        fcookie;
    xcb_query_font_cookie_t  icookie;
    xcb_generic_error_t     *error;

    font->id = xcb_generate_id(c);
    fcookie = xcb_open_font_checked(
        c, font->id, strlen(name), name);
    icookie = xcb_query_font(c, font->id);
    error = xcb_request_check(c, fcookie);
    if(error)
    {
        fprintf(stderr, "Cannot open font '%s' (%d)\n",
                config.font, error->error_code);
        return -1;
    }

    font->info = xcb_query_font_reply(c, icookie, NULL);
    if(!font->info)
        return -1;

    if(!xcb_query_font_char_infos_length(font->info))
        font->data = NULL;
    else
        font->data = xcb_query_font_char_infos(font->info);

    font_fillinfo(font);

    return 0;
}

void
font_free(font_t font)
{
    xcb_close_font(c, font.id);
    if(font.info)
        free(font.info);
}
