#include <stdlib.h>
#include <string.h>
#include "transition.h"
#include "config.h"

config_s config;

static uint32_t
get_colorpixel(const char *hex)
{
    char strgroups[3][3] = {
        { hex[1], hex[2], '\0' },
        { hex[3], hex[4], '\0' },
        { hex[5], hex[6], '\0' }
    };

    uint8_t r = strtol(strgroups[0], NULL, 16);
    uint8_t g = strtol(strgroups[1], NULL, 16);
    uint8_t b = strtol(strgroups[2], NULL, 16);

    return (0xFF << 24) | (r << 16 | g << 8 | b);
}

#define STYLE_SET(X, Border, Bg, Fg, BorderSize, Opacity) \
    X.border = get_colorpixel(Border);                    \
    X.background = get_colorpixel(Bg);                    \
    X.foreground = get_colorpixel(Fg);                    \
    X.border_size = BorderSize;                           \
    X.opacity = (Opacity / 100.) * 0xFFFFFFFF;

void
config_init()
{
    STYLE_SET(config.style[URGENCY_LOW],
            "#333333", "#222222", "#888888", 1, 70);
    STYLE_SET(config.style[URGENCY_NORMAL],
            "#4C7899", "#222222", "#FFFFFF", 1, 85);
    STYLE_SET(config.style[URGENCY_CRITICAL],
            "#900000", "#302222", "#FFDDDD", 2, 100);
    config.padding       = 2;
    config.margin        = 10;
    config.expire        = 5.;
    config.above         = 1;
    config.transition    = 0.5;
    config.cols          = 25;
    config.lines         = 5;
    config.default_width = 150;
    config.port          = REMOTE_DEFAULT_PORT;
    config.ip            = NULL;
    config.password      = NULL;
    config.logfile       = NULL; //"/tmp/test.log";
    config.hidepassword  = 1;
    config.transon       = transition_none;
    config.transoff      = transition_none;
    strcpy(config.font, "fixed");
            /*"Bitstream Sans Vera Mono:pixelsize=12:antialias=true,fixed");*/
}
