export CC      = clang
export CFLAGS  = -Wall -Wextra -pipe -O2 # -g -DDEBUG
export LDFLAGS =

export PREFIX  = /usr/
export EXEC    = notifire
export VERSION = 0.42a

export SERVER  = ${EXEC}d
export CLIENT  = ${EXEC}c

all: ${SERVER} sep ${CLIENT}

run: all
	@./${SERVER} -df

server: ${SERVER}


${SERVER}:
	@make -sC server
	@mv server/${SERVER} .

client: ${CLIENT}


${CLIENT}:
	@make -sC client
	@mv client/${CLIENT} .

clean:
	@make -sC server clean
	@make -sC client clean

nuke: clean
	@echo -e "\e[01;34mRemoving binaries\e[0m"
	@rm -f ${SERVER}
	@rm -f ${CLIENT}

install:
	@mkdir -p ${PREFIX}/bin/
	@echo -e "\e[01;32mInstalling ${SERVER} binary\e[0m"
	@install -s -m 755 ${SERVER} ${PREFIX}/bin/
	@ln -s ${PREXFIX}/bin/${SERVER} ${PREFIX}/bin/lenotifieurdefeu
	@echo -e "\e[01;32mInstalling ${CLIENT} binary\e[0m"
	@install -s -m 755 ${CLIENT} ${PREFIX}/bin/
	@echo -e "\e[01;32m${EXEC} installed\e[0m"

uninstall:
	@echo -e "\e[01;32mRemoving ${SERVER} binary\e[0m"
	@rm -f ${PREFIX}/bin/${SERVER}
	@rm -f ${PREFIX}/bin/lenotifieurdefeu
	@echo -e "\e[01;32mRemoving ${CLIENT} binary\e[0m"
	@rm -f ${PREFIX}/bin/${CLIENT}
	@echo -e "\e[01;32m${EXEC} uninstalled\e[0m"

test: all
	@echo -e "\e[01;32mTesting...\e[0m"
	@./${SERVER} -r test@0.0.0.0:24242 &
	@sleep 1
	@notify-send -u low -c tayst DBus "This is a dbus notification"
	@echo -e "This\nIs\nA LF\nTest" | xargs -0 notify-send "DBus \n"
	@./notifirec -r test@localhost:filesphere -u normal -c tayst "Remote" "This is a notification sent remotely in the default binary format"
	@./notifirec -r test@0.0.0.0:24242 -j -t 2 -u critical -c tayst "Remote Json" "This is a notification sent remotely in json"
	@echo -e "\e[01;32mWaiting...\e[0m"
	@read
	@killall ${SERVER}
	@echo -e "\e[01;32mEnd...\e[0m"

sep:
	@echo -e ""

.PHONY: all run info install uninstall test clean ${SERVER} ${CLIENT}
