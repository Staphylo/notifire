#ifndef REMOTE_HDR_H
#define REMOTE_HDR_H

#include <stdint.h>

typedef struct remote_hdr_s *remote_hdr_t;
struct remote_hdr_s
{
    uint8_t id;
    uint8_t opcode;
    uint16_t size;
} __attribute__ ((packed));

/*#pragma pack(push,1)
typedef struct remote_hdr_s
{
    uint8_t id;
    uint8_t opcode;
    uint16_t size;
} *remote_hdr_t;
#pragma pack(pop)*/

#define REMOTE_DEFAULT_PORT 42058
#define REMOTE_HDR_LEN      sizeof(struct remote_hdr_s)
#define REMOTE_MAX_BUF_LEN  2048

enum {
    REMOTE_OP_END      = 0x00,
    REMOTE_OP_PASSWORD = 0x01,
    REMOTE_OP_TITLE    = 0x02,
    REMOTE_OP_MESSAGE  = 0x03,
    REMOTE_OP_EXPIRE   = 0x04,
    REMOTE_OP_ICON     = 0x05,
    REMOTE_OP_URGENCY  = 0x06,
    REMOTE_OP_CATEGORY = 0x07,
    REMOTE_OP_APPNAME  = 0x08
};

#endif /* !REMOTE_HDR_H */
